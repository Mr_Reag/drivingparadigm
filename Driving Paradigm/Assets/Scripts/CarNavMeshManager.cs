﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class CarNavMeshManager : MonoBehaviour
{

    [HideInInspector]
    public List<Transform> navMeshPoints;


    private NavMeshAgent _navMeshAgent;
    private int currentNavPoint = 0;

    void Awake() //if we havent manually set the navmeshpoints, do so
    {
        if (navMeshPoints != null) return;
        navMeshPoints = new List<Transform>();
        
    }


	// Use this for initialization
	void Start ()
	{
	    _navMeshAgent = GetComponent<NavMeshAgent>();
	    //_navMeshAgent.autoBraking = false;

	    MoveToNextPoint();
	    Invoke("SetupNavLinks",.1F);
	}

    private void SetupNavLinks()
    {
        FindObjectsOfType<NavMeshLink>().ToList().ForEach(x => x.UpdateLink());
    }

    void MoveToNextPoint()
    {

        if (currentNavPoint >= navMeshPoints.Count)
        {

            //_navMeshAgent.isStopped = true;
            return;
        }

         Debug.Log(currentNavPoint);
        Debug.Log(navMeshPoints.Count);

        if (currentNavPoint + 1 == navMeshPoints.Count)
        {
            _navMeshAgent.autoBraking = true; //slow down for the end

        }

        _navMeshAgent.destination = navMeshPoints[currentNavPoint].position;
        currentNavPoint++;
    }

	
	// Update is called once per frame
	void Update () {
	    if (_navMeshAgent.remainingDistance < 5F)
	    {
            MoveToNextPoint();
	    }
    }
}
