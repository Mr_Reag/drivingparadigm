﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshLoader : MonoBehaviour {

    public NavMeshData navMeshData;
    private NavMeshDataInstance _navMeshInstance;


    void Start ()
    {
        navMeshData.position = transform.position;
        navMeshData.rotation = transform.rotation;
        _navMeshInstance = NavMesh.AddNavMeshData(navMeshData);
    }

    void OnDisable()
    {
        NavMesh.RemoveNavMeshData(_navMeshInstance);
    }
}
