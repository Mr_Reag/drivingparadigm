﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

/// <summary>
/// This class chases after the provided navmesh agent
/// </summary>
public class CarNavChaser : MonoBehaviour
{


    public GameObject target;
    private CarController _controller;


	// Use this for initialization
	void Start ()
	{
	    _controller = GetComponent<CarController>();
	    if (_controller == null || target == null)
	    {
            Debug.LogError("Target or Car Controller misconfigured! Experiment will not run properly");
            Destroy(this);
	    }
	}
	
	// Update is called once per frame
	void Update () {
		//_controller.
	}
}
