﻿using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class CityBuilder : MonoBehaviour
{

    public List<GameObject> cityPrefabSections;


    //Pick if we use the nav mesh or the waypoint builder
    public bool useWaypoint = true;
    private GameObject _circuitGameObject;
    private WaypointCircuit _circuit;
    private int _waypointNum = 0;

    private static CityBuilder _cityBuilder; //reference to this

    private Dictionary<string, Transform> targetLocationPoints;

    //[HideInInspector]
    public List<int> sectionIDList; //the list of city sections the car will pass through

    private GameObject _spawnLocation;
    private CarNavMeshManager _carNavManager;

    //object for locking the singleton
    private object l = new object();
    void Awake()
    {
        //create singleton of the city builder
        lock (l)
        {
            if (_cityBuilder != null)
                Destroy(this);

            _cityBuilder = this;
        }

        if (useWaypoint)
        {
            _circuitGameObject = new GameObject("Waypoint Circuit");
            _circuit = _circuitGameObject.AddComponent<WaypointCircuit>();
        }  
        targetLocationPoints = new Dictionary<string, Transform>();
    }


// Use this for initialization
	void Start ()
	{
	    var handoff = FindObjectOfType<DriveSceneHandoff>();
        if(handoff == null)
	        BuildCity();
	}

    public void BuildCity()
    {
        var car = GameObject.FindGameObjectWithTag("Car");

        _carNavManager = car.GetComponent<CarNavMeshManager>(); //get our car

        //Make the first waypoint the start
        if (useWaypoint)
        {
            var p = new GameObject("StartPoint");
            p.transform.position = car.transform.position;
            p.transform.SetParent(_circuitGameObject.transform);
        }


        _spawnLocation = new GameObject("prefabSpawnPoint");
        _spawnLocation.transform.parent = transform; //create a spawnpoint for our prefabs

        foreach (int i in sectionIDList) //then, for every section we want to create
        {
            var spawned = Instantiate(cityPrefabSections[i], transform); //we instantiate that section and move it into position
            spawned.transform.position = _spawnLocation.transform.position;
            spawned.transform.rotation = _spawnLocation.transform.rotation;

            Transform endpointTransform = null;
            Transform connectionTransform = null;

            targetLocationPoints = new Dictionary<string, Transform>();

            for (int j = 0; j < spawned.transform.childCount; j++) //then we find its NavSectionEndpoint
            {
                var child = spawned.transform.GetChild(j);


                if (child.tag != "NavSectionEndpoint" && child.tag != "ConnectionPoint" && child.tag != "NavSection") continue;
                if (child.tag == "NavSectionEndpoint")
                {
                    endpointTransform = child;
                }
                else if (child.tag == "NavSection") //temp disable this 
                {
                    targetLocationPoints.Add(child.name, child.transform); //This is a bit of a hack. NavSection's must ALWAYS be named an integer number that represents its order
                }
                else
                {
                    connectionTransform = child;
                }
            }

            if (endpointTransform == null || connectionTransform == null) continue; //we failed to find a NavSectionEndpoint

            //Finally, we update the spawn location to connect and the rotation to match the endpoint, so we can spawn another section
            _spawnLocation.transform.position = connectionTransform.position;
            _spawnLocation.transform.rotation = endpointTransform.rotation;

            //And tell our car to head to that location
            for (int k = 0; k < targetLocationPoints.Count; k++)
            {
                var p = targetLocationPoints[k.ToString()]; //add each point to the navmesh
                if (useWaypoint)
                {
                    p.name = "Waypoint " + (_waypointNum++).ToString("000");
                    p.SetParent(_circuitGameObject.transform);

                }
                else
                    _carNavManager.navMeshPoints.Add(p);
            }

            if (useWaypoint)
            {
                endpointTransform.name = "Waypoint " + (_waypointNum++).ToString("000");
                endpointTransform.SetParent(_circuitGameObject.transform);
                _circuit.AddAllChildrenToWaypointList();

            }
            else
                _carNavManager.navMeshPoints.Add(endpointTransform); //and the endpoint
        }

        if (useWaypoint)
        {
            WaypointProgressTracker wpt = car.GetComponent<WaypointProgressTracker>();
            wpt.circuit = _circuit;
            wpt.Reset();
        }
    }
}
