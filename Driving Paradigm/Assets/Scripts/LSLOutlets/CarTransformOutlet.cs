﻿using System.Collections;
using System.Collections.Generic;
using Assets.LSL4Unity.Scripts;
using UnityEngine;

public class CarTransformOutlet : LSLTransformOutlet {


    //wrappr for custom logic for the car transform.

    //Override LateUpdate to not send data when source is null
    void LateUpdate()
    {
        if (outlet == null || sampleSource == null)
            return;

        sample();
    }
}
