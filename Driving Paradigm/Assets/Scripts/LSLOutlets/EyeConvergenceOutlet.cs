﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.LSL4Unity.Scripts;
using Assets.LSL4Unity.Scripts.Common;
using LSL;
using UnityEngine;
using System.Linq;

public class EyeConvergenceOutlet : MonoBehaviour {

    private const string unique_source_id_suffix = "D256CFBDBA3BDB23";

    private string unique_source_id;

    private liblsl.StreamOutlet outlet;
    private liblsl.StreamInfo streamInfo;
    public liblsl.StreamInfo GetStreamInfo()
    {
        return streamInfo;
    }
    /// <summary>
    /// Use a array to reduce allocation costs
    /// </summary>
    private float[] currentSample;

    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    private const double dataRate = liblsl.IRREGULAR_RATE;


    public double GetDataRate()
    {
        return dataRate;
    }


    //do we have someone listening?
    public bool HasConsumer()
    {
        return outlet != null && outlet.have_consumers();
    }

    public string StreamName = "Unity.Driving.EyeConvergance";
    public string StreamType = "Unity.EyeConvergance";
    private const int ChannelCount = 10;

    public MomentForSampling sampling;

    public Transform carTransform; //Used to find the relative transform compared to the car, as world space is useless


    public GameObject FOVERig;
    private FoveInterface foveHeadset;

    public bool debugLogging = false;

    private bool isInvalid = false;

    void Awake()
    {
        // assigning a unique source id as a combination of a the instance ID for the case that
        // multiple LSLTransformOutlet are used and a guid identifing the script itself.
        unique_source_id = string.Format("{0}_{1}", GetInstanceID(), unique_source_id_suffix);
    }

    void Start()
    {
        //check if FOVE is connected
        bool connected;
        try
        {
            connected = FoveInterface.IsHardwareConnected();
        }
        catch (Exception)
        {
            connected = false;
        }

        if (!connected && !debugLogging)
        {
            Debug.Log("FOVE not connected. Removing eye vector LSL outlet");
            Destroy(this);
            return;
        }


        // initialize the array once
        currentSample = new float[ChannelCount];

        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, ChannelCount, dataRate, liblsl.channel_format_t.cf_float32, unique_source_id);

        outlet = new liblsl.StreamOutlet(streamInfo);


        foveHeadset = FOVERig.GetComponentInChildren<FoveInterface>();
    }

    /// <summary>
    /// This method pushes our EyeConv data sample.
    /// </summary>
    private void pushSample()
    {
        //Dont push data if we dont have the car!
        if (isInvalid || carTransform == null)
            return;

        if (FoveInterface.IsEyeTrackingCalibrating())
            return;

        var eyeDirLeft = FoveInterface.GetLeftEyeVector();
        var eyeDirRight = FoveInterface.GetRightEyeVector();

        //Convergance Calc. We check for the current bound object first, as reading throws off the convergence number


        var dat = foveHeadset.GetWorldGazeConvergence();
        var convPoint = dat.ray.GetPoint(dat.distance); //the convergance point in world space

        //TODO: DEBUG this to make sure your using the correct type on inverse transform. It might be a InverseTransformDirection instead
        eyeDirLeft = carTransform.InverseTransformVector(eyeDirLeft); //Turn the vector from world space to local space, relative to the car
        eyeDirRight = carTransform.InverseTransformVector(eyeDirRight); //Turn the vector from world space to local space, relative to the car
        convPoint = carTransform.InverseTransformPoint(convPoint); //This one is probably correct. Its a point in space relative to the car

        //Debug.Log("Eye Vector: " + eyeDirLeft); //debug to be sure that this is working correctly


        //Debug.Log("Attension Value: " + FoveInterfaceBase.GetAttentionValue());
        //Debug.Log("Pupil Dil: " + dat.pupilDilation);


        //Eye Vector (left eye)
        currentSample[0] = eyeDirLeft.x;
        currentSample[1] = eyeDirLeft.y;
        currentSample[2] = eyeDirLeft.z;

        //Eye Vector (Right eye)
        currentSample[3] = eyeDirRight.x;
        currentSample[4] = eyeDirRight.y;
        currentSample[5] = eyeDirRight.z;

        //Real convergance point, in local space relative to the car
        currentSample[6] = convPoint.x;
        currentSample[7] = convPoint.y;
        currentSample[8] = convPoint.z;

        //pupilSize
        currentSample[9] = dat.pupilDilation;

        outlet.push_sample(currentSample, liblsl.local_clock());

        if (debugLogging) Debug.Log(string.Join(",", currentSample.Select(x => x.ToString()).ToArray()));
        //Debug.Log("Pushed EyeConv Sample");
    }

    /*
    * Do our sampling 
    */
    void FixedUpdate()
    {
        if (sampling == MomentForSampling.FixedUpdate)
            pushSample();
    }

    void Update()
    {
        if (sampling == MomentForSampling.Update)
            pushSample();
    }

    void LateUpdate()
    {
        if (sampling == MomentForSampling.LateUpdate)
            pushSample();
    }
}
