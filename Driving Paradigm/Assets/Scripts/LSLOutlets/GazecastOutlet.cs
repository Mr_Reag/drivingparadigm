﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.LSL4Unity.Scripts;
using LSL;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class GazecastOutlet : MonoBehaviour {

    private const string unique_source_id_suffix = "D256CFBDBDDA3BDB23";

    private string unique_source_id;

    private liblsl.StreamOutlet outlet;
    private liblsl.StreamInfo streamInfo;
    public liblsl.StreamInfo GetStreamInfo()
    {
        return streamInfo;
    }
    /// <summary>
    /// Use a array to reduce allocation costs
    /// </summary>
    private string[] currentSample;

    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    private const double dataRate = liblsl.IRREGULAR_RATE;


    public double GetDataRate()
    {
        return dataRate;
    }


    //do we have someone listening?
    public bool HasConsumer()
    {
        return outlet != null && outlet.have_consumers();
    }

    public string StreamName = "Unity.Driving.Gazecast";
    public string StreamType = "Unity.Gazecast";
    private const int ChannelCount = 1;

    public MomentForSampling sampling;

    public GameObject FOVERig;
    private FoveInterface foveHeadset;

    private Dictionary<Collider,string> _foveColliderDict;

    public bool debugLogging = false;

    private bool isInvalid = false;

    void Awake()
    {
        // assigning a unique source id as a combination of a the instance ID for the case that
        // multiple LSLTransformOutlet are used and a guid identifing the script itself.
        unique_source_id = string.Format("{0}_{1}", GetInstanceID(), unique_source_id_suffix);

        _foveColliderDict = new Dictionary<Collider, string>();
    }

    void Start()
    {
        //check if FOVE is connected
        bool connected;
        try
        {
            connected = FoveInterface.IsHardwareConnected();
        }
        catch (Exception)
        {
            connected = false;
        }

        if (!connected && !debugLogging)
        {
            Debug.Log("FOVE not connected. Removing gazecast LSL outlet");
            Destroy(this);
            return;
        }

        Invoke("SetupColliderList",.1F);

        // initialize the array once
        currentSample = new string[ChannelCount];

        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, ChannelCount, dataRate, liblsl.channel_format_t.cf_string, unique_source_id);
         
        outlet = new liblsl.StreamOutlet(streamInfo);

        foveHeadset = FOVERig.GetComponentInChildren<FoveInterface>();

        isInvalid = true;

    }

    public void SetupCollider()
    {
        Invoke("SetupColliderList", .5F);
    }

    private void SetupColliderList()
    {
        GameObject.FindGameObjectsWithTag("Road").Select(x => x.GetComponent<Collider>()).ToList().ForEach(x =>
        {
            _foveColliderDict.Add(x, "Road");
        });
        GameObject.FindGameObjectsWithTag("Building").Select(x => x.GetComponent<Collider>()).ToList().ForEach(x =>
        {
            _foveColliderDict.Add(x, "Building");
        });

        isInvalid = false;
    }


    /// <summary>
    /// This method pushes our EyeConv data sample.
    /// </summary>
    private void pushSample()
    {
        if (isInvalid) //wait for us to be setup
            return;

        if (FoveInterface.IsEyeTrackingCalibrating())
            return;

        //Find which collider, if any, the user is looking at
        Collider which;
        currentSample[0] = "M";

        foveHeadset.Gazecast(_foveColliderDict.Keys, out which);

        //If its one we're tracking,
        if (which != null) currentSample[0] = _foveColliderDict[which]; //get the tag
        else return; //dont push empty samples
        outlet.push_sample(currentSample, liblsl.local_clock()); //and push it

        if (debugLogging) Debug.Log(currentSample[0]);
        //Debug.Log("Pushed EyeConv Sample");
    }

    /*
    * Do our sampling 
    */
    void FixedUpdate()
    {
        if (sampling == MomentForSampling.FixedUpdate)
            pushSample();
    }

    void Update()
    {
        if (sampling == MomentForSampling.Update)
            pushSample();
    }

    void LateUpdate()
    {
        if (sampling == MomentForSampling.LateUpdate)
            pushSample();
    }
}
