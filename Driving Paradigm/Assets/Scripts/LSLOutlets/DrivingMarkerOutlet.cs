﻿using System.Collections;
using System.Collections.Generic;
using Assets.LSL4Unity.Scripts;
using LSL;
using UnityEngine;

public class DrivingMarkerOutlet : MonoBehaviour
{
    private static DrivingMarkerOutlet _instance;

    private string unique_source_id;

    private const string unique_source_id_suffix = "D3F83BB699EB44B88882AB";

    public string lslStreamName = "Unity.Driving.Markers";
    public string lslStreamType = "Marker";

    private liblsl.StreamInfo lslStreamInfo;
    private liblsl.StreamOutlet lslOutlet;
    private int lslChannelCount = 1;

    //Assuming that markers are never send in regular intervalls
    private double nominal_srate = liblsl.IRREGULAR_RATE;

    private const liblsl.channel_format_t lslChannelFormat = liblsl.channel_format_t.cf_string;

    private string[] sample;



    public bool debugLogging = false;

    private static object l = new object();
    void Awake()
    {
        lock (l)
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        sample = new string[lslChannelCount];

        lslStreamInfo = new liblsl.StreamInfo(
            lslStreamName,
            lslStreamType,
            lslChannelCount,
            nominal_srate,
            lslChannelFormat,
            unique_source_id);

        lslOutlet = new liblsl.StreamOutlet(lslStreamInfo);


        // assigning a unique source id as a combination of a the instance ID for the case that
        // multiple LSLTransformOutlet are used and a guid identifing the script itself.
        unique_source_id = string.Format("{0}_{1}", GetInstanceID(), unique_source_id_suffix);
    }

    public static void WriteMarker(string marker)
    {
        if (_instance == null)
        {
            Debug.LogError("Marker Stream not initialized yet");
            return;
        }
        _instance.WriteCustomMarker(marker);
    }

    private void WriteCustomMarker(string marker)
    {
        Write(marker);
        if(debugLogging) Debug.Log("MARKER: " + marker);
    }


    public void Write(string marker)
    {
        sample[0] = marker;
        lslOutlet.push_sample(sample);
    }
}
