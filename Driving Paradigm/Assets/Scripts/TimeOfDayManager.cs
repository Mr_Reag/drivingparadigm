﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[ExecuteInEditMode]
public class TimeOfDayManager : MonoBehaviour
{

    public Light[] lightSources;
    public Material[] skyboxMaterials;

    public int timeOfDay = 1;
    private int _currentTime = -1;



	// Sets the skybox
	void Start () {
	    if (timeOfDay != _currentTime)
            SetUpSkybox();
	}
	
	// Update is used to preview in editor
	void Update () {
        if(timeOfDay != _currentTime)
		    SetUpSkybox();
	}


    public void SetUpSkybox()
    {
        foreach (var l in lightSources)
        {
            l.gameObject.SetActive(false);
        }
        if (timeOfDay >= lightSources.Length || lightSources.Length != skyboxMaterials.Length)
        {
            Debug.LogError("Error setting up skybox and sessionDuration. Light sources and materials are misconfigured");
            return;
        }

        var lightSource = lightSources[timeOfDay];

        lightSource.gameObject.SetActive(true);

        RenderSettings.skybox = skyboxMaterials[timeOfDay];
        RenderSettings.sun = lightSource;

        Invoke(lightSource.gameObject.name.Contains("Night") ? "TurnOnLights" : "TurnOffLights", .1F);

        _currentTime = timeOfDay;


    }


    private void TurnOnLights()
    {
        //Debug.Log("Turning on lamps");
        GameObject.FindGameObjectsWithTag("LampLight").ToList().ForEach(x => x.GetComponent<Light>().enabled = true);
    }

    private void TurnOffLights()
    {
        //Debug.Log("Turning off lamps");
        GameObject.FindGameObjectsWithTag("LampLight").ToList().ForEach(x => x.GetComponent<Light>().enabled = false);
    }
}
