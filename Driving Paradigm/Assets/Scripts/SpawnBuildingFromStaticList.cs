﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBuildingFromStaticList : MonoBehaviour
{

    public List<GameObject> spawnList; //spawn list of prefabs. If null, is ignored. If not null, first processed one becomes the static list
    private static List<GameObject> _staticSpawnList; //the static list of prefabs that will be spawned

    public bool setAsStaticList = false; //ONLY ONE INSTANCE OF THIS SCRIPT SHOULD HAVE THIS SET TO TRUE!


    public bool _useStatic = true;

    private object l = new object();//lock


    //We use awake to set up a static list that all instances of this script will use. This way, we only need to define our list once
    void Awake()
    {
        //Critical Section!
        lock (l)
        {
            if (spawnList == null || spawnList.Count == 0) return;
            if (_staticSpawnList == null && setAsStaticList)
            {
                _staticSpawnList = spawnList;
            }
            else if(setAsStaticList)
            {
                Debug.LogError("Multiple valid lists can be used to build the static building prefabs! One of these lists was discarded! this is not how this script works!");
            }
            else
            {
                _useStatic = false;
            }
        }
    }


    // Use this for initialization
    void Start()
    {
        if (_staticSpawnList == null && _useStatic)
        {
            Debug.LogError("Attempting to use the static list, but the static list isnt set up!");
            Destroy(this);
            return;
        }
        float xRand = Random.Range(0, 10F) - 5F; //Pick a random between -5 and 5 for x and z
        float zRand = Random.Range(0, 10F) - 5F;

        var offset = new Vector3(xRand,0,zRand);

        int buildingCount = (_staticSpawnList == null) ? spawnList.Count : _staticSpawnList.Count;

        int buildingToSpawn = Random.Range(0, buildingCount - 1); //Pick a random building to spawn

        var prefab = (_useStatic) ? _staticSpawnList[buildingToSpawn] : spawnList[buildingToSpawn];

        var building = Instantiate(prefab, transform); //spawn in our building

        building.transform.localPosition = building.transform.localPosition + offset; //give it some randomness in location
    }
	
}
