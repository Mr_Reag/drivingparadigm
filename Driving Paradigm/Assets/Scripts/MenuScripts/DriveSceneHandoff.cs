﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq.Expressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Runtime.InteropServices;
using UnityStandardAssets.Vehicles.Car;
using Debug = UnityEngine.Debug;

public class DriveSceneHandoff : MonoBehaviour
{
    private static readonly object o = new object();

    //reference to singleton
    public static DriveSceneHandoff Instance { get; private set; }
    private static GameObject _activeFoveRig; //reference to active scene camera


    //Save Data
    public static MenuData _data { get; private set; }


    //Reference to menu
    [SerializeField]
    private MainInterfaceBehavior menuBehavior;

    [SerializeField] private TMP_InputField fileLocInput;

    private static bool _foveDetected = false;


    // Use this for initialization
    void Awake () {
	    lock (o)
	    {
	        if (Instance == null)
	        {
	            Instance = this;
                DontDestroyOnLoad(gameObject);
	            _data = new MenuData();
                SetupFove();
	        }
	        else
	        {
                Destroy(this);
	        }
	    }

	}

    private void SetupFove()
    {
        try
        {
            _activeFoveRig = GameObject.FindGameObjectWithTag("ActiveFOVERig");
            DontDestroyOnLoad(_activeFoveRig);//set our FOVE Rig to be persistant
            //_foveDetected = FoveInterface.IsHardwareConnected(); this is broken on FOVE's end.
            _foveDetected = true;
            if(!_foveDetected) Debug.LogWarning("No FOVE Detected!");
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            Debug.LogError("Error With FOVE! Disabling all FOVE Functions!");
            _foveDetected = false;
        }
    }


    public void SaveData()
    {
        if (_data.DrivingTrack.Equals(""))
        {
            Debug.LogWarning("Attempted to save an empty track! I assume this is not what you intended. Save Aborted!");
            return;
        }

        string fileLoc = fileLocInput.text;
        if (fileLoc.Equals(""))
        {
            fileLoc = "/drivingconfig.dat";
            fileLocInput.text = fileLoc;
        }
        string destination = Application.persistentDataPath + fileLoc;

        var file = File.Exists(destination) ? File.OpenWrite(destination) : File.Create(destination);

        var bf = new BinaryFormatter();
        bf.Serialize(file, _data);
        file.Close();
    }

    public void LoadData()
    {
        string fileLoc = fileLocInput.text;
        if (fileLoc.Equals(""))
        {
            fileLoc = "/drivingconfig.dat";
            fileLocInput.text = fileLoc;
        }
        string destination = Application.persistentDataPath + fileLoc;
        FileStream file;

        if (File.Exists(destination)) file = File.OpenRead(destination);
        else
        {
            Debug.LogError("File not found");
            return;
        }

        BinaryFormatter bf = new BinaryFormatter();
        _data = (MenuData) bf.Deserialize(file);
        file.Close();

        UpdateMenuText();
    }


    private void UpdateMenuText()
    {
        menuBehavior.UpdateMenuValues(_data.SessionDuration, _data.DelayDuration,_data.TimeOfDay, _data.CrusingSpeed, _data.AccelerationForce, _data.BrakingForce, _data.HeadsetHeight, _data.DrivingTrack);
    }



    public void UpdateSessionDuration(string newTime)
    {
        _data.SessionDuration = float.Parse(newTime);
    }

    public void UpdateDelayDuration(string newDelay)
    {
        _data.DelayDuration = float.Parse(newDelay);
    }

    public void UpdateTimeOfDay(int tod)
    {
        _data.TimeOfDay = tod;
    }

    public void UpdateCrusingSpeed(string kmh)
    {
        _data.CrusingSpeed = float.Parse(kmh);
    }

    public void UpdateAccForce(string force)
    {
        _data.AccelerationForce = float.Parse(force);
    }

    public void UpdateBrakeForce(string force)
    {
        _data.BrakingForce = float.Parse(force);
    }

    public void UpdateHeadsetHeight(string h)
    {
        _data.HeadsetHeight = float.Parse(h);
    }



    public void UpdateTrack(string track)
    {
        if(Regex.IsMatch(track, @"^([0-9];)*[0-9]$"))
            _data.DrivingTrack = track;
        else
        {
            Debug.LogWarning("Invalid TrackSetup!");
        }
    }


    public void LoadDrivingScene()
    {
        StartCoroutine(AsyncSceneLoad());
    }


    private static IEnumerator AsyncSceneLoad()
    {
        var load = SceneManager.LoadSceneAsync("DriveScene"); //async load the next scene
        load.allowSceneActivation = false;

        //Dont finish loading until we finish the eye calibration
        if (_foveDetected)
        {
            bool callibrating = true;
            while (callibrating)
            {
                FoveInterfaceBase.GetFVRHeadset().IsEyeTrackingCalibrating(out callibrating);
                //Debug.Log(callibrating);
                yield return new WaitForSecondsRealtime(.1F);
            }
        }


        //Wait for scene to be fully loaded before we swap
        yield return new WaitUntil(() => load.progress >= .9F);
        load.allowSceneActivation = true;
        //Find the new scene
        var sc = SceneManager.GetSceneByName("DriveScene");
        yield return new WaitUntil(() => sc.isLoaded); //and wait for it to fully render before setting it up!

        //Log session data, then setup the scene
        SetupDrivingScene();
        yield return null;
    }

    //sets up all the parameters for the new scene
    private static void SetupDrivingScene()
    {
        //Start by sending the required Markers
        DrivingMarkerOutlet.WriteMarker("Scene Loaded with Parameters: " + _data);


        //Set Time Of Day
        var todM = FindObjectOfType<TimeOfDayManager>();
        todM.timeOfDay = _data.TimeOfDay; //set the time of day. Note there are issues with lighting currently. Working to fix

        //Create the City
        var builder = FindObjectOfType<CityBuilder>();
        builder.sectionIDList = _data.DrivingTrack.Split(';').Select(int.Parse).ToList();// split the string by ; and use it to build a list of track sections
        builder.BuildCity(); //actually build the city
        
        //Setup the Car
        var car = FindObjectOfType<CarController>();
        car.m_Topspeed = _data.CrusingSpeed; //the topspeed
        car.m_FullTorqueOverAllWheels = _data.AccelerationForce; //how much force we can use to accelerate to top speed
        car.m_BrakeTorque = _data.BrakingForce*2; //This break is used mostly for turns
        car.m_MaxHandbrakeTorque = _data.BrakingForce; //Handbreak is only used when we come to a complete stop

        //Set up the Session Timer
        float delay = _data.DelayDuration;
        float dur = delay + _data.SessionDuration;
        var aiControl = FindObjectOfType<CarAIControl>();
        aiControl.SetTimerTillStop(dur);
        aiControl.StopDriving();
        aiControl.StartDrivingAfterDelay(delay);

        //Setup markers for those events
        Instance.StartCoroutine(Instance.MarkerPassthrough("Start Of Session Signal",delay));
        Instance.StartCoroutine(Instance.MarkerPassthrough("End Of Session Signal",dur));

        var toDest = GameObject.FindGameObjectsWithTag("ActiveFOVERig").First(x => !x.Equals(_activeFoveRig)); //find the debug cam, if it exists
        var transf = (toDest != null) ? toDest.transform : _activeFoveRig.transform; //get the target transform and setup the FOVE cam
        _activeFoveRig.transform.parent = transf.parent;

        //Note the FOVE Debug camera is 1 meter off the ground, so we need to offset its position
        _activeFoveRig.transform.position = transf.position + Vector3.up * (_data.HeadsetHeight - 1);
        _activeFoveRig.transform.rotation = transf.rotation;

        var carTransform = GameObject.FindGameObjectWithTag("Car").transform;
        var carOutlet = FindObjectOfType<CarTransformOutlet>();
        var eyeOutlet = FindObjectOfType<EyeConvergenceOutlet>();
        var gazeOutlet = FindObjectOfType<GazecastOutlet>();

        //setup our outlets so they now have the car, and can start sending useful data
        carOutlet.sampleSource = carTransform;
        eyeOutlet.carTransform = carTransform;
        gazeOutlet.SetupCollider();

        //And we destroy the debug camera,as we dont need it
        if (toDest != null) Destroy(toDest);

    }
    
    //This is nessessary, as the CarAIController is in its own namespace, and cannot access any of the LSL untilities
    IEnumerator MarkerPassthrough(string marker, float time)
    {
        yield return new WaitForSecondsRealtime(time);
        DrivingMarkerOutlet.WriteMarker(marker);
    }

}
