﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;

public class MainInterfaceBehavior : MonoBehaviour, ISubmitHandler
{
    [SerializeField] private List<GameObject> panels;
    [SerializeField] private GameObject saveLoadPanel;

    private int _activePanel;


    //References to menu
    public TMP_InputField simTimeField;
    public TMP_InputField delTimeField;
    public TMP_Dropdown todDropdown;
    public TMP_InputField csField;
    public TMP_InputField accField;
    public TMP_InputField decField;
    public TMP_InputField hhField;
    public TMP_InputField trackField;
    
    
    

    void Awake()
    {
        panels[0].SetActive(true);
        panels[1].SetActive(false);
        panels[2].SetActive(false);

        _activePanel = 0;
    }

    public void UpdateMenuValues(float dur,float del, int tod, float cs, float acc, float dec, float hh, string track)
    {
        simTimeField.text = "" + dur;
        delTimeField.text = "" + del;
        todDropdown.value = tod;
        csField.text = "" + cs;
        accField.text = "" + acc;
        decField.text = "" + dec;
        hhField.text = "" + hh;
        trackField.text = track;
    }

    public void SaveData()
    {
        DriveSceneHandoff.Instance.SaveData();
    }

    public void LoadData()
    {
        DriveSceneHandoff.Instance.LoadData();
    }

    public void OnSubmit(BaseEventData eventData)
    {
        if (_activePanel >= 0) OnPageForward(eventData);
        Debug.Log("Submit");
    }

    private void OnPageForward(BaseEventData eventData)
    {
        panels[_activePanel].SetActive(false);
        panels[++_activePanel].SetActive(true);
        if(_activePanel >= 2) saveLoadPanel.SetActive(false);

        /*
        Debug.Log("To Track Input");
        panels[0].SetActive(false);
        panels[1].SetActive(true);
        */
    }

    public void OnPageBack(BaseEventData eventData)
    {
        Debug.Log("Back to parameters");
        panels[0].SetActive(true);
        panels[1].SetActive(false);
    }

}
