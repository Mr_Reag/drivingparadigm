﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MenuData  {

    public float SessionDuration { get; set; }
    public float DelayDuration { get; set; }
    public int TimeOfDay { get; set; }
    public float CrusingSpeed { get; set; }
    public float AccelerationForce { get; set; }
    public float BrakingForce { get; set; }
    public float HeadsetHeight { get; set; }
    public string DrivingTrack { get; set; }


    public MenuData()
    {

    }

    public MenuData(float sd, float dd,int tod, float cs, float af, float bf, float hh, string track)
    {
        SessionDuration = sd;
        DelayDuration = dd;
        TimeOfDay = tod;
        CrusingSpeed = cs;
        AccelerationForce = af;
        BrakingForce = bf;
        HeadsetHeight = hh;
        DrivingTrack = track;
    }


    public override string ToString()
    {
        string ret = "";
        ret = string.Format("({0},{1},{2},{3},{4},{5},{6})\n{7}", SessionDuration,DelayDuration, TimeOfDay, CrusingSpeed,
            AccelerationForce, BrakingForce, HeadsetHeight, DrivingTrack);
        return ret;
    }
}
